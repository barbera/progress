#!/usr/bin/python

from __future__ import print_function
import json
import sys
import time

if len(sys.argv) < 2:
    sys.exit("Usage:\n  set-status.py init\n  set-status.py ip:ip-address status:running message:message")

def writeJson (js):
    try:
        fhOut = open("cgi-bin/status.json", "w")
    except:
        sys.exit("cannot create file")

    print(json.dumps(js, indent=4))
    print(json.dumps(js, indent=4), file=fhOut)

    fhOut.close()

def initializeJson ():
    js = dict()

    js["pipeline"] = "someversion"

    js["ip"] = "1.2.3.4"

    js["data"] = dict()
    js["data"]["reference"] = "IGH_HUMAN"
    js["data"]["samples"] = ["S1","S2","S3","S4"]

    js["progress"] = [{'timestamp':time.strftime("%Y-%m-%d %H:%M:%S"), 'ip':"1.2.3.4", 'status':'INITIALIZING', 'message': 'Initializing progress monitor'}]

    return(js)

def addToJson (args):
    # Read json file
    try:
        fhIn = open("cgi-bin/status.json", "r")
    except:
        sys.exit("cannot open file")

    text = fhIn.read()
    js = json.loads(text)

    fhIn.close()

    # Add to json and write to the same file
    js_tmp = dict()
    js_tmp['timestamp'] = time.strftime("%Y-%m-%d %H:%M:%S")
    for inp in args[1:]:
        [key, value] = inp.split(":")
        js_tmp[key] = value
    js["progress"].append(js_tmp)

    return(js)

####### Main ############

if sys.argv[1] == "init":
    js = initializeJson()
    writeJson(js)
else:
    js = addToJson(sys.argv)
    writeJson(js)
