#!/usr/bin/python

from __future__ import print_function
import json
import sys
import os
thisdir = os.getcwd()

print("Content-type: application/javascript\n")

jsonFile = thisdir + "/cgi-bin/status.json"

try:
    fhIn = open(jsonFile, "r")
except:
    print("cannot open file")
    sys.exit("cannot open file")

text = fhIn.read()
js = json.loads(text)

print(json.dumps(js))
