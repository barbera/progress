#!/usr/bin/python2

from __future__ import print_function
import urllib
import json
import os
import time

ipFile = open("ip-list", "rb")

js = list()
js_all = list()
for ip in ipFile:
    ip = str(ip.rstrip())
    url = "http://" + ip + ":8000/cgi-bin/get-status.py"

    try:
        fh = urllib.urlopen(url)
        text = fh.read()
        js_tmp = json.loads(text)
        js.append(js_tmp["progress"][-1])  # get last status
        js_all.append(js_tmp)   # get all info
    except:
        js_tmp = {"timestamp":time.strftime("%Y-%m-%d %H:%M:%S"), "ip": ip, "status": "UNKNOWN", "message": "couldn't retrieve status"}
        js.append(js_tmp)

for item in js:
    print(item["timestamp"], item["ip"], item["status"], item["message"])

fhOut = open("status-all.json", "w")
print(json.dumps(js_all, indent=4), file=fhOut)
fhOut.close()
