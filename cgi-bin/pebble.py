#!/usr/bin/python

from __future__ import print_function
import urllib
import json
import os
import time

print("Content-type: application/javascript\n")

ipFile = open("ip-list", "rb")

# Setup json for pebble
js_pebble = dict()
js_pebble["refresh"] = 300
js_pebble["vibrate"] = 0
js_pebble["font"] = 4
js_pebble["theme"] = 0
js_pebble["scroll"] = 100
js_pebble["light"] = 0
js_pebble["blink"] = 0
js_pebble["updown"] = 0
js_pebble["auth"] = "salt"
js_pebble["content"] = ""

# Get all status info
status_count = dict()
for ip in ipFile:
    ip = ip.rstrip()
    url = "http://" + ip + ":8000/cgi-bin/get-status.py"

    try:
        fh = urllib.urlopen(url)
        text = fh.read()
        js_tmp = json.loads(text)
        status = js_tmp["progress"][-1]["status"]
        status_count[status] = status_count.get(status, 0) + 1
        js_pebble["content"] = js_pebble["content"] + ip + " " + status + "\n"
    except:
        js_tmp = {"timestamp":time.strftime("%Y-%m-%d %H:%M:%S"), "ip": ip, "status": "UNKNOWN", "message": "couldn't retrieve status"}
        status = "UNKNOWN"
        status_count["UNKNOWN"] = status_count.get("UNKNOWN", 0) + 1
        js_pebble["content"] = js_pebble["content"] + ip + " UNKNOWN\n"

js_pebble["content"] = js_pebble["content"] + "=====\n"

# Get statuses and counts
for status in status_count:
    js_pebble["content"] = js_pebble["content"] + str(status_count[status]) + " " + status + "\n"

# Send alerts when there are no running jobs left and set the refresh rate on 4 hours
if status_count.get("RUNNING", 0) == 0 and status_count.get("INITIALIZING", 0) == 0:
    js_pebble["vibrate"] = 1
    js_pebble["light"] = 1
    js_pebble["blink"] = 3
    js_pebble["refresh"] = 14400

# Dump json
print(json.dumps(js_pebble, indent=4))
fhOut = open("cgi-bin/pebble.json", "w")
print(json.dumps(js_pebble, indent=4), file=fhOut)
fhOut.close()
