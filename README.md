# README #

Job monitor that was originally developed for the [RESEDA](https://bitbucket.org/barbera/reseda) pipeline. Handy if you run multiple jobs simultaneously on cloud or another system. It keeps track of the progress of the analysis and can be used for other applications too. The jobs can be monitored from a laptop or via a smartwatch.

### Required ###
The possibility to broadcast its status via the web. The job monitor runs on port 8000.

### How to ###
On the machine where the analysis takes place:

* Start the progress monitor: screen ./run.sh (with "ctrl+a d" it runs in the background)
* Initialize the monitor (from your analysis script): ./set-status.py init
* Change status (do this in the analysis script): ./set-status ip:1.2.3.4 status:RUNNING message:"Aligning stuff"

Monitor all jobs from another (virtual) machine or from your laptop:

* List all the ip-addresses that you want to monitor in "ip-list"
* Run ./display.py

Job monitoring on a pebble smartwatch:

* Install "My data" by Bahbka on your watch
* Connect to the machine that monitors all jobs: http(s)://domain.org:8000/cgi-bin/pebble.py

### On the to-do list ###

To be added to the json file

* git version of the data analysis code that is executed
* list of samples
* name of reference dataset

### Author ###

* Barbera DC van Schaik, b.d.vanschaik@amc.uva.nl

### License ###

```
Progress - it monitors jobs
Copyright (C) 2016 Barbera DC van Schaik

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see (http://www.gnu.org/licenses/).
```